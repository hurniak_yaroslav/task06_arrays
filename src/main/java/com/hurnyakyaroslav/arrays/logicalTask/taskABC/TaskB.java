package com.hurnyakyaroslav.arrays.logicalTask.taskABC;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Arrays;

public class TaskB {
    public static Logger logger = LogManager.getLogger(TaskB.class);
    public static int[] masuv;

    TaskB() {
        masuv = new int[10];
        for (int i = 0; i < masuv.length; i++) {
            masuv[i] = (int) (Math.random() * 100);
        }

    }

    private int isRepeated(int elem) {
        int counter = 0;
        for (int i : masuv) {
            if (i == elem) {
                counter++;
            }
        }
        return counter;
    }

    private boolean isInMasuv(int element, int[] mas) {
        for (int i : mas) {
            if (element == i) return true;
        }
        return false;
    }


    int counter = 0;
    int allCounter = 0;

    private int[] getPopularNumbers() {
        int length = 0;

        for (int i = 0; i < masuv.length; i++) {
            if (isRepeated(masuv[i]) >= 3) length += isRepeated(masuv[i]);
        }
        int[] popularNumbers = new int[length / 3];
//        int counter = 0;
//        int allCounter = 0;
        for (int i = 0; i < masuv.length; i++) {
            if (isRepeated(masuv[i])>=3) {
                allCounter++;
                if (!isInMasuv(masuv[i], popularNumbers)) {
                    popularNumbers[counter] = masuv[i];
                    counter++;
                }

            }
        }
        return popularNumbers;
    }

    public void removePopularNumbers() {
        int[] popularNumbers = getPopularNumbers();
        int[] newMasuv = new int[masuv.length - allCounter];
        for (int i = 0, j = 0; i < masuv.length; i++) {
            if (!isInMasuv(masuv[i], popularNumbers)) {
                newMasuv[j] = masuv[i];
                j++;
            }
        }
        masuv = newMasuv;
    }


    public static void main(String[] args) {
        TaskB taskB = new TaskB();
      logger.info(Arrays.toString(taskB.masuv));

        taskB.removePopularNumbers();
        logger.info(Arrays.toString(taskB.masuv));
    }
}
