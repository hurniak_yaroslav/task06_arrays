package com.hurnyakyaroslav.arrays.logicalTask.taskD.controller;

import com.hurnyakyaroslav.arrays.logicalTask.taskD.model.Hall;

public class Game {
    Hall hall = new Hall();


    public String getInfo() {
        return hall.getInfo();
    }

    public int calculateDeadDoors() {
        return hall.calculateDoorsOfDead(0, hall.getDoors().iterator());
    }

    public String surviveDoorsArray(){
        return  hall.surviveDoors();
    }

}
