package com.hurnyakyaroslav.arrays.logicalTask.taskD;

import com.hurnyakyaroslav.arrays.logicalTask.taskD.view.ConsoleView;

public class Application {
    public static void main(String[] args) {
        new ConsoleView().view();
    }
}
