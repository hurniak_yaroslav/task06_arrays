package com.hurnyakyaroslav.arrays.logicalTask.taskD.model;

public class Door {
    private Hidden hidden;

    public Door() {
        if (Math.random() > 0.5) hidden = new Monster();
        else hidden = new Artifact();
    }

    public int open() {
        return hidden.interuct();
    }

    @Override
    public String toString() {
        return String.format(" %10s\t\t %d",
                hidden.getClass().getSimpleName(), hidden.hp);

    }
}
