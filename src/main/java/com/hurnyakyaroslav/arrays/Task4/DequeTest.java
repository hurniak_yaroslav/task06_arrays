package com.hurnyakyaroslav.arrays.Task4;

import com.hurnyakyaroslav.arrays.Task3.Task3;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayDeque;
import java.util.Deque;

public class DequeTest {

    public static Logger logger = LogManager.getLogger(DequeTest.class);

    public Deque<Integer> deque;
    DequeTest(){
        deque = new ArrayDeque<>();
    }


    public static void main(String[] args) {
        DequeTest dequeTest = new DequeTest();
        for(int i=0; i<10; i++){
            dequeTest.deque.addFirst(i);
            dequeTest.deque.addLast(i);
        }
        logger.info(dequeTest.deque.toString());
    }
}
