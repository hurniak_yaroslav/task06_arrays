package com.hurnyakyaroslav.arrays.Task2;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

import static java.lang.System.currentTimeMillis;

public class CustomStringArray {

    public static Logger logger = LogManager.getLogger(CustomStringArray.class);

    private int capacity;
    private int length;
    private String[] customArray;

    CustomStringArray() {
        capacity = 3;
        length = 0;
        customArray = new String[capacity];

    }

    public void add(String str) {
        if (length == capacity) resize();
        customArray[length] = str;
        length++;
    }

    public String get(int index) {
        return customArray[index];
    }

    private void resize() {
        capacity += capacity;
        String[] newStringArray = new String[capacity];
        for (int i = 0; i < customArray.length; i++) {
            newStringArray[i] = customArray[i];
        }
        customArray = newStringArray;
    }

    public static void main(String[] args) {


        CustomStringArray customStringArray = new CustomStringArray();
        long startTime = currentTimeMillis();
        customStringArray.add("one");
        customStringArray.add("two");
        customStringArray.add("three");
        customStringArray.add("four");
        customStringArray.add("five");


        for (int i = 0; i < customStringArray.length; i++) {
            logger.info(customStringArray.get(i));
        }
        long endTime = currentTimeMillis();
        logger.info("Running time of own array: " + (endTime-startTime));




        List<String> arraylist = new ArrayList();
        startTime = currentTimeMillis();
        arraylist.add("one");
        arraylist.add("two");
        arraylist.add("three");
        arraylist.add("four");
        arraylist.add("five");
        for (int i = 0; i < arraylist.size(); i++) {
            logger.info(arraylist.get(i));
        }
        endTime = currentTimeMillis();
        logger.info("Running time of ArrayList: " + (endTime-startTime));

    }
}
